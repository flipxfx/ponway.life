function Youtube ({ id }) {
  return (
    <iframe src={`https://www.youtube.com/embed/${id}?rel=0&iv_load_policy=3`} allowFullScreen='' frameBorder='0' />
  )
}

export default Youtube
